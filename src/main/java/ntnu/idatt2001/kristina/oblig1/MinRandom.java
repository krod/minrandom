package ntnu.idatt2001.kristina.oblig1;
import java.util.Random;
import java.util.Scanner;

class MinRandom {
    // Objektvariablar
    private final static Random tilfeldig = new Random();

    // Metodar
    public static int getNesteHeiltal(int nedre, int ovre) {
        return tilfeldig.nextInt(ovre - nedre) + nedre;
    }

    public static double getNesteDesimaltal(double nedre, double ovre) {
        return tilfeldig.nextDouble() * (ovre - nedre) + nedre;
    }

    // Dette er ein kommentar som utgjer ei endring i koden.
    // Og ein til.
    // Den tredje

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Velg om du vil ha eitt tilfeldig  heiltal eller desimaltal. \n(1) Heiltal \n(2) Desimaltal");
        int valg = in.nextInt();

        switch (valg) {
            case 1: // Heiltal
                System.out.print("Angi den nedre intervallgrensa: ");
                int nedreHeiltal = in.nextInt();
                System.out.print("Angi den øvre intervallgrensa: ");
                int ovreHeiltal = in.nextInt();
                int tilfeildigHeiltal = MinRandom.getNesteHeiltal(nedreHeiltal, ovreHeiltal);
                System.out.println(tilfeildigHeiltal + " er eit tilfeldig heiltal mellom " + nedreHeiltal + " og " + ovreHeiltal);
                break;

            case 2: // Desimaltal
                System.out.print("Angi den nedre intervallgrensa: ");
                double nedreDesimaltal = in.nextDouble();
                System.out.print("Angi den øvre intervallgrensa: ");
                double ovreDesimaltal = in.nextDouble();
                double tilfeldigDesimaltal = MinRandom.getNesteDesimaltal(nedreDesimaltal, ovreDesimaltal);
                System.out.println(tilfeldigDesimaltal + " er eit tilfeldig desimaltal mellom " + nedreDesimaltal + " og " + ovreDesimaltal + " :)");
                break;

            default:
                System.out.print("Du må velje anten (1) heiltal eller (2) desimaltal.");
        }
    }
}